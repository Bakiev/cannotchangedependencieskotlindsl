plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-android-extensions")
}

android {
    compileSdkVersion(29)

    defaultConfig {
        minSdkVersion(16)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
    }
}

dependencies {
}

// FIX #2 - rename:
// module-one/build.gradle.kts -> module-one/build.gradle.kts
// module-one/build.gradle.back -> module-one/build.gradle
